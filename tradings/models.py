from django.db import models
from django.urls import reverse

from django.contrib.auth import get_user_model

User = get_user_model()

# Create your models here.

class Category(models.Model):
    title = models.CharField(max_length=20)

    def get_absolute_url(self):
        return reverse('tradings:category-detail', kwargs={'pk': self.pk})

    
    def __str__(self):
        return self.title
    


class Item(models.Model):
    title = models.CharField(max_length=20)
    category = models.ForeignKey(Category, related_name='items', on_delete=models.CASCADE)
    user = models.ForeignKey(User, related_name='items', null=True, on_delete=models.SET_NULL)


    def get_absolute_url(self):
        return reverse('tradings:item-detail', kwargs={'pk': self.pk})


    def __str__(self):
        return self.title
    