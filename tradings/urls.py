from django.urls import path

from . import views

app_name = 'tradings'

urlpatterns = [
    path('', views.Index.as_view(), name='index'),
    path('categories/<int:pk>/', views.CategoryDetail.as_view(), name='category-detail'),
    path('categories/', views.CategoryList.as_view(), name='category-list'),
]
