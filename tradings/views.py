from django.shortcuts import render
from django.views.generic import (
    TemplateView,
    ListView,
    DetailView
)

from .models import Item, Category

# Create your views here.


class Index(TemplateView):
    template_name = 'tradings/index.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        user = self.request.user
        if user.is_authenticated:
            context['items'] = Item.objects.filter(user=user)
        return context


class CategoryList(ListView):
    model = Category

class CategoryDetail(DetailView):
    model = Category
