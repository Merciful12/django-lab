from django.contrib.auth.forms import UserCreationForm
from django.views.generic import FormView


class RegisterFormView(FormView):
    form_class = UserCreationForm
    success_url = 'accounts/login'
    template_name = 'accounts/register.html'


    def form_valid(self, form):
        form.save()
        return super(RegisterFormView, self).form_valid(form)
